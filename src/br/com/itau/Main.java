package br.com.itau;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.sql.SQLOutput;

import java.util.Map;


public class Main {

    public static void main(String[] args) {
        InputOutput.imprimirMensagemIncial();
        Map<String , String> dados = InputOutput.solicitarDados();

        Cliente cliente1 = new Cliente(
                dados.get("nome"),
                dados.get("cpf"),
                Integer.parseInt(dados.get("idade"))
        );

        System.out.println(cliente1.getNome());

        if(cliente1.verificaIdade(cliente1.getIdade())) {
            System.out.println("maior de idade");
        }else{
            System.out.println("Cliente menor de idade, não é possível criar conta");
            System.exit(0);
        }



        Conta conta1 = new Conta ();
        conta1.setCliente(cliente1);
        conta1.setAgencia(9999);
        conta1.setConta(1111);
        conta1.setDigito(1);
        conta1.setSaldo(0.0);

        conta1.Deposita(100.0);

        System.out.println(conta1.getSaldo());

        conta1.Saca(50.0);

        System.out.println(conta1.getSaldo());

        conta1.Saca(20.0);

        System.out.println(conta1.getSaldo());

        System.out.println("-----------------");
        System.out.println("CPF =   "+cliente1.getCpf());
        System.out.println("Nome =  "+cliente1.getNome());
        System.out.println("Idade = "+cliente1.getIdade());
        System.out.println("Saldo = "+conta1.getSaldo());
        System.out.println("-----------------");
    }
}

