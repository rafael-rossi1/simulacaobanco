package br.com.itau;

public class Conta {
    private Cliente cliente;
    private int agencia;
    private int conta;
    private int digito;
    private double saldo;

    public Conta() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getConta() {
        return conta;
    }

    public void setConta(int conta) {
        this.conta = conta;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void Deposita(double valorDeposito) {
        if(valorDeposito <= 0) {
            System.out.println("Valor inválido para depósito");
        }
        else {
            saldo = saldo + valorDeposito;
        }
    }

    public void Saca(double valorSaque) {
        if(saldo <= 0) {
            System.out.println("Saldo insuficiente");
        }
        else {
            saldo = saldo - valorSaque;
        }
    }
}
