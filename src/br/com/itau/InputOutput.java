package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InputOutput {
    public static void imprimirMensagemIncial() {
        System.out.println("Bem vindo ao sistema");
    }

    public static Map<String, String> solicitarDados() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite seu nome: ");
        String nome = scanner.nextLine();

        System.out.println("Digite seu cpf: ");
        String cpf = scanner.nextLine();

        System.out.println("Digite sua idade: ");
        Integer idade = scanner.nextInt();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("idade", idade.toString());

        return dados;
    }
}

